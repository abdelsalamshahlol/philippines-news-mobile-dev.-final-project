package com.news.abdelsalamshahlol.finalprojectnewsapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import model.LoadJSONTask;
import model.News;

public class MainActivity extends AppCompatActivity implements LoadJSONTask.Listener, AdapterView.OnItemClickListener {
    ListView lv;
    CustomAdapter ad;

    public static final String URL = "https://newsapi.org/v2/top-headlines?country=ph&apiKey=7b62a7a52042410a83cb78113dff1c52";

    private List<HashMap<String, String>> mNewsMapList = new ArrayList<>();

    private static final String KEY_Title = "title";
    private static final String KEY_Description = "description";
    private static final String KEY_URL = "url";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_UrlToImg = "urlToImg";
    private static final String Key_PUBLISHEDDATE = "datePublished";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.lv1);
        lv.setOnItemClickListener(this);
        registerForContextMenu(this.lv);
        new LoadJSONTask(this).execute(URL);
    }


    //this method used to create the context menu called confirm
    @Override
    public void onCreateContextMenu(ContextMenu confirm, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(confirm, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.confirm, confirm);
    }

    //this is for context menu actions
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
//        System.out.println("HERE   "+info.id);
        int listPos = info.position;
        switch (item.getItemId()) {
            case R.id.browser:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mNewsMapList.get(listPos).get(KEY_URL)));
                startActivity(intent);
                break;

            case R.id.del:
                try {
                    mNewsMapList.remove(listPos);
                     ad.notifyDataSetChanged();
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                break;

            default:
                Toast.makeText(null, "Something is not right @_@", Toast.LENGTH_SHORT).show();
                break;
        }

        return true;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //open the url on browser based on user click
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mNewsMapList.get(i).get(KEY_URL)));
        startActivity(intent);
    }


    @Override
    public void onLoaded(List<News> newsList) {
        for (News articles : newsList) {

            HashMap<String, String> map = new HashMap<>();

            map.put(KEY_Title, articles.getTitle());
            map.put(KEY_URL, articles.getUrl());
            //this methods strips the HTML from the description
            map.put(KEY_Description, android.text.Html.fromHtml(articles.getDescription()).toString().replaceAll("\n", "").trim());
            //if the source is empty this places N/A
            String source = (articles.getAuthor() == null || articles.getAuthor() == "" ? "Not Available" : articles.getAuthor());
            map.put(KEY_AUTHOR, source);
            map.put(KEY_UrlToImg, articles.getUrlToImage());
            String date = articles.getPublishedAt().substring(0, articles.getPublishedAt().indexOf("T"));
            map.put(Key_PUBLISHEDDATE, date);

            mNewsMapList.add(map);
        }
        //hide the loading gif using custom class
        pl.droidsonroids.gif.GifTextView imgView = findViewById(R.id.loadingImage);
        imgView.setVisibility(View.GONE);
        loadListView();

    }

    @Override
    public void onError() {
        Toast.makeText(this, "Error !", Toast.LENGTH_SHORT).show();
    }

    String[] from = {KEY_Title, KEY_Description, KEY_AUTHOR, Key_PUBLISHEDDATE, KEY_UrlToImg, KEY_URL};
    int[] to = {R.id.title, R.id.desc, R.id.source, R.id.date, R.id.img};

    private void loadListView() {
         ad = new CustomAdapter(this, mNewsMapList);
        lv.setAdapter(ad);

    }

}
