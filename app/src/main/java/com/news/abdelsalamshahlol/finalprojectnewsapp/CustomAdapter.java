package com.news.abdelsalamshahlol.finalprojectnewsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.HashMap;
import java.util.List;

public class CustomAdapter extends BaseAdapter {

    private Activity mActivity;
    private List<HashMap<String, String>> mNewsDetails;

    public CustomAdapter(Activity mActivity, List<HashMap<String, String>> mNewsDetails) {
        this.mActivity = mActivity;
        this.mNewsDetails = mNewsDetails;
    }

    @Override
    public int getCount() {
        return mNewsDetails.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolderItem mViewHolderItem;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.lv, parent, false);

            mViewHolderItem = new ViewHolderItem();
            mViewHolderItem.img =  convertView.findViewById(R.id.img);
            mViewHolderItem.source =  convertView.findViewById(R.id.source);
            mViewHolderItem.title =  convertView.findViewById(R.id.title);
            mViewHolderItem.date = convertView.findViewById(R.id.date);
            mViewHolderItem.desc = convertView.findViewById(R.id.desc);

            convertView.setTag(mViewHolderItem);
        } else {
            mViewHolderItem = (ViewHolderItem) convertView.getTag();
        }
//        System.out.println("HERE   "+mNewsDetails.get(position).toString());
//        System.out.println("Here url for each image"+mNewsDetails.get(position).get("urlToImg").toString());
        mViewHolderItem.title.setText(mNewsDetails.get(position).get("title"));
        mViewHolderItem.desc.setText(mNewsDetails.get(position).get("description"));
        mViewHolderItem.date.setText(mNewsDetails.get(position).get("datePublished"));
        mViewHolderItem.source.setText(mNewsDetails.get(position).get("author"));
        Picasso.with(convertView.getContext())
                    .load(mNewsDetails.get(position).get("urlToImg"))
                    .resize(300, 200)
                    .into(mViewHolderItem.img);

        return convertView;
    }

    //inner class with view widgets used in the customized list view
    static class ViewHolderItem {
        private ImageView img;
        private TextView source, title,date,desc;
    }
}